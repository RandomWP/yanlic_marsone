import flask
from data import db_session, user_table
import werkzeug

blueprint = flask.Blueprint("users_api", __name__, template_folder="templates")


@blueprint.route("/api/users", methods=("GET",))
def get_users():
    session = db_session.create_session()
    users = session.query(user_table.User).all()
    return flask.jsonify(
        {
            "users": [item.to_dict() for item in users]
        }
    )


@blueprint.route("/api/users/<int:user_id>", methods=("GET",))
def get_user(user_id: int):
    session = db_session.create_session()
    user = session.query(user_table.User).get(user_id)
    if not user:
        return flask.jsonify({"error": "user not found"})
    else:
        return flask.jsonify(
            {
                "user": user.to_dict()
            }
        )


@blueprint.route("/api/users", methods=("POST",))
def add_user():
    req_keys = ("surname", "name", "age", "position",
                "speciality", "address", "email", "password")
    session = db_session.create_session()
    if not flask.request.json:
        return flask.jsonify({"error": "empty request"})
    elif not all(map(lambda key: key in flask.request.json, req_keys)):
        return flask.jsonify({"error": "bad request"})
    elif session.query(user_table.User).filter(user_table.User.email == flask.request.json["email"]).first():
        return flask.jsonify({"error": "user alredy exists"})
    else:
        user = user_table.User()
        user.surname = flask.request.json["surname"]
        user.name = flask.request.json["name"]
        user.age = flask.request.json["age"]
        user.position = flask.request.json["position"]
        user.speciality = flask.request.json["speciality"]
        user.address = flask.request.json["address"]
        user.email = flask.request.json["email"]
        user.set_passwd(flask.request.json["password"])
        session.add(user)
        session.commit()
        return flask.jsonify({"success": "ok"})


@blueprint.route("/api/users/<int:user_id>", methods=("DELETE",))
def delete_user(user_id: int):
    session = db_session.create_session()
    user = session.query(user_table.User).get(user_id)
    if not user:
        return flask.jsonify({"error": "not found"})
    else:
        session.delete(user)
        session.commit()
        return flask.jsonify({"error": "ok"})


@blueprint.route("/api/users/<int:user_id>", methods=("PUT",))
def edit_user(user_id: int):
    req_keys = ("surname", "name", "age", "position",
                "speciality", "address", "email", "password")
    session = db_session.create_session()
    user = session.query(user_table.User).get(user_id)
    if not user:
        return flask.jsonify({"error": "not found"})
    elif not all(map(lambda key: key in flask.request.json, req_keys)):
        return flask.jsonify({"error": "bad request"})
    else:
        user.surname = flask.request.json["surname"]
        user.name = flask.request.json["name"]
        user.age = flask.request.json["age"]
        user.position = flask.request.json["position"]
        user.speciality = flask.request.json["speciality"]
        user.address = flask.request.json["address"]
        user.email = flask.request.json["email"]
        user.set_passwd(flask.request.json["password"])
        session.commit()
        return flask.jsonify({"success": "ok"})
