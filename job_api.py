import flask
from data import db_session, job_table, user_table

blueprint = flask.Blueprint("jobs_api", __name__, template_folder="templates")


@blueprint.route("/api/jobs")
def get_jobs():
    session = db_session.create_session()
    jobs = session.query(job_table.Jobs).all()
    return flask.jsonify(
        {
            "jobs": [item.to_dict(rules=("-user", "-categories")) for item in jobs]
        }
    )


@blueprint.route("/api/jobs/<int:job_id>", methods=("GET",))
def get_job(job_id: int):
    session = db_session.create_session()
    job = session.query(job_table.Jobs).get(job_id)
    if not job:
        return flask.jsonify({"error": "not found"})
    else:
        return flask.jsonify(
            {
                "job": job.to_dict(rules=("-user", "-categories"))
            }
        )


@blueprint.route("/api/jobs", methods=("POST",))
def add_job():
    if flask.request.method == "POST":
        req_keys = ("team_leader", "job", "work_size",
                    "collaborators", "is_finished")
        session = db_session.create_session()
        if not flask.request.json:
            return flask.jsonify({"error": "no_content"})
        elif not all(map(lambda key: key in flask.request.json, req_keys)):
            return flask.jsonify({"error": "bad_request"})
        elif "id" in flask.request.json and session.query(job_table.Jobs.id).filter(job_table.Jobs.id == flask.request.json["id"]).scalar() is not None:
            return flask.jsonify({"error": "id_already_exists"})
        job = job_table.Jobs(**flask.request.json)
        session.add(job)
        session.commit()
        return flask.jsonify({"success": "ok"})


@blueprint.route("/api/jobs/<int:job_id>", methods=("DELETE",))
def del_job(job_id: int):
    session = db_session.create_session()
    job = session.query(job_table.Jobs).get(job_id)
    if not job:
        return flask.jsonify({"error": "not found"})
    else:
        session.delete(job)
        session.commit()
        return flask.jsonify({"success": "ok"})


@blueprint.route("/api/jobs/<int:job_id>", methods=("PUT",))
def put_job(job_id: int):
    req_keys = ("team_leader", "job", "work_size",
                "collaborators", "is_finished")
    session = db_session.create_session()
    job = session.query(job_table.Jobs).get(job_id)
    if not job:
        return flask.jsonify({"error": "not found"})
    elif not all(map(lambda key: key in flask.request.json, req_keys)):
        return flask.jsonify({"error": "bad request"})
    else:
        job.team_leader = flask.request.json["team_leader"]
        job.job = flask.request.json["job"]
        job.work_size = flask.request.json["work_size"]
        job.collaborators = flask.request.json["collaborators"]
        job.is_finished = flask.request.json["is_finished"]
        session.commit()
        return flask.jsonify({"success": "ok"})
