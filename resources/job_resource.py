import flask_restful
import flask
from data import job_table, db_session
from resources.job_parser import parser


def abort_if_not_found(id):
    session = db_session.create_session()
    job = session.query(job_table.Jobs).get(id)
    if not job:
        flask_restful.abort(404, message=f"Job with id {id} not found")


class JobResource(flask_restful.Resource):
    def get(self, job_id):
        abort_if_not_found(job_id)
        session = db_session.create_session()
        job = session.query(job_table.Jobs).get(job_id)
        return flask.jsonify({"job": job.to_dict(rules=("-user", "-categories"))})

    def put(self, job_id):
        abort_if_not_found(job_id)
        args = parser.parse_args()
        session = db_session.create_session()
        job = session.query(job_table.Jobs).get(job_id)
        job.team_leader = args["team_leader"]
        job.job = args["job"]
        job.work_size = args["work_size"]
        job.collaborators = args["collaborators"]
        job.start_date = args["start_date"]
        job.end_date = args["end_date"]
        job.is_finished = args["is_finished"]
        session.commit()
        return flask.jsonify({"success": "ok"})

    def delete(self, job_id):
        abort_if_not_found(job_id)
        session = db_session.create_session()
        job = session.query(job_table.Jobs).get(job_id)
        session.delete(job)
        session.commit()
        return flask.jsonify({"success": "ok"})


class JobListResource(flask_restful.Resource):
    def get(self):
        session = db_session.create_session()
        jobs = session.query(job_table.Jobs).all()
        return flask.jsonify(
            {
                "jobs": [item.to_dict(rules=("-user", "-categories")) for item in jobs]
            }
        )

    def post(self):
        args = parser.parse_args()
        session = db_session.create_session()
        job = job_table.Jobs(
            team_leader=args["team_leader"],
            job=args["job"],
            work_size=args["work_size"],
            collaborators=args["collaborators"],
            start_date=args["start_date"],
            end_date=args["end_date"],
            is_finished=args["is_finished"]
        )
        session.add(job)
        session.commit()
        return flask.jsonify({"success": "ok"})
