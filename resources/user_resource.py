import flask_restful
import flask
from data import user_table, db_session
from resources.user_parser import parser


def abort_if_not_found(id):
    session = db_session.create_session()
    user = session.query(user_table.User).get(id)
    if not user:
        flask_restful.abort(404, message=f"User with id {id} not found")


class UserResource(flask_restful.Resource):
    def get(self, user_id):
        abort_if_not_found(user_id)
        session = db_session.create_session()
        user = session.query(user_table.User).get(user_id)
        return flask.jsonify({"user": user.to_dict(rules=("-jobs", "-departments"))})

    def put(self, user_id):
        args = parser.parse_args()
        abort_if_not_found(user_id)
        session = db_session.create_session()
        user = session.query(user_table.User).get(user_id)
        user.surname = args["surname"]
        user.name = args["name"]
        user.age = args["age"]
        user.position = args["position"]
        user.speciality = args["speciality"]
        user.address = args["address"]
        user.email = args["email"]
        user.set_passwd(args["password"])
        session.commit()
        return flask.jsonify({"success": "ok"})

    def delete(self, user_id):
        abort_if_not_found(user_id)
        session = db_session.create_session()
        user = session.query(user_table.User).get(user_id)
        session.delete(user)
        session.commit()
        return flask.jsonify({"success": "ok"})


class UserListResource(flask_restful.Resource):
    def get(self):
        session = db_session.create_session()
        users = session.query(user_table.User).all()
        return flask.jsonify(
            {
                "users": [item.to_dict(rules=("-jobs", "-departments")) for item in users]
            }
        )

    def post(self):
        args = parser.parse_args()
        session = db_session.create_session()
        user = user_table.User(
            surname=args["surname"],
            name=args["name"],
            age=args["age"],
            position=args["position"],
            speciality=args["speciality"],
            address=args["address"],
            email=args["email"]
        )
        user.set_passwd(args["password"])
        session.add(user)
        session.commit()
        return flask.jsonify({"success": "ok"})
