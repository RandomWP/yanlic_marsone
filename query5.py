filename = input()
global_init(filename)
session = create_session()
jobs = session.query(Jobs).filter(
    Jobs.work_size < 20, ~Jobs.is_finished)
for job in jobs:
    print(job)
