import flask_wtf
import wtforms
from wtforms import validators


class LoginForm(flask_wtf.FlaskForm):
    email = wtforms.fields.html5.EmailField(
        "Email", validators=[validators.Email(), validators.DataRequired()])
    password = wtforms.PasswordField(
        "Password", validators=[validators.DataRequired()])
    remember = wtforms.BooleanField("Remember me")
    submit = wtforms.SubmitField("Log in")
