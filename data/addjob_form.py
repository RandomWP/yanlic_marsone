import flask_wtf
import wtforms
from wtforms import validators


class AddJobForm(flask_wtf.FlaskForm):
    job = wtforms.StringField("Job title", validators=[
                              validators.DataRequired()])
    team_leader = wtforms.IntegerField("Teamleader id", validators=[
                                       validators.DataRequired()])
    work_size = wtforms.IntegerField("Work size", validators=[
                                     validators.DataRequired()])
    collaborators = wtforms.StringField("Collaborators")
    category = wtforms.IntegerField("Hazard category", validators=[
                                    validators.DataRequired()])
    is_finished = wtforms.BooleanField("Job finished")
    submit = wtforms.SubmitField("Add")
