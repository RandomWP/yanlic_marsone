import datetime
import sqlalchemy
from .db_session import SqlAlchemyBase
from sqlalchemy_serializer import SerializerMixin
from flask_login import UserMixin
import werkzeug


class User(SqlAlchemyBase, SerializerMixin, UserMixin):
    __tablename__ = "users"
    id = sqlalchemy.Column(
        sqlalchemy.Integer, primary_key=True, autoincrement=True)
    surname = sqlalchemy.Column(sqlalchemy.String)
    name = sqlalchemy.Column(sqlalchemy.String)
    age = sqlalchemy.Column(sqlalchemy.Integer)
    position = sqlalchemy.Column(sqlalchemy.String)
    speciality = sqlalchemy.Column(sqlalchemy.String)
    address = sqlalchemy.Column(sqlalchemy.String)
    email = sqlalchemy.Column(sqlalchemy.String, unique=True)
    hashed_password = sqlalchemy.Column(sqlalchemy.String)
    modified_date = sqlalchemy.Column(
        sqlalchemy.DateTime, default=datetime.datetime.now)
    city_from = sqlalchemy.Column(sqlalchemy.String)
    jobs = sqlalchemy.orm.relation("Jobs", back_populates="user")
    departments = sqlalchemy.orm.relation("Departments", back_populates="user")

    def set_passwd(self, passwd: str):
        self.hashed_password = werkzeug.security.generate_password_hash(passwd)

    def check_passwd(self, passwd: str):
        return werkzeug.security.check_password_hash(self.hashed_password, passwd)
