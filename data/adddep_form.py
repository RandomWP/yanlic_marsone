import flask_wtf
import wtforms
from wtforms import validators


class AddDepForm(flask_wtf.FlaskForm):
    title = wtforms.StringField("Department title", validators=[
                                validators.DataRequired()])
    chief = wtforms.IntegerField("Chief of the department", validators=[
                                 validators.DataRequired()])
    members = wtforms.StringField(
        "Members", validators=[validators.DataRequired()])
    email = wtforms.fields.html5.EmailField(
        "Email", validators=[validators.Email(), validators.DataRequired()])
    submit = wtforms.SubmitField("Add")
