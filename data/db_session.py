import sqlalchemy
import sqlalchemy.ext.declarative

SqlAlchemyBase = sqlalchemy.ext.declarative.declarative_base()

__factory = None


def global_init(db_file):
    global __factory

    if __factory:
        return

    if not db_file or not db_file.strip():
        raise FileNotFoundError("No file selected")

    conn_str = f'sqlite:///{db_file.strip()}?check_same_thread=False'
    print(f"Connect to db at {conn_str}")

    engine = sqlalchemy.create_engine(conn_str, echo=False)
    __factory = sqlalchemy.orm.sessionmaker(bind=engine)

    from . import __all_models

    SqlAlchemyBase.metadata.create_all(engine)


def create_session() -> sqlalchemy.orm.Session:
    global __factory
    return __factory()
