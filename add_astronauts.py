from data import db_session, user_table

ast_data = (
    {
        "surname": "Scott",
        "name": "Ridley",
        "age": 21,
        "position": "captain",
        "speciality": "research engineer",
        "address": "module_1",
        "email": "scott_chief@mars.org",
        "city_from": "Washington"
    },
    {
        "surname": "Merkurev",
        "name": "Max",
        "age": 16,
        "position": "worker",
        "speciality": "software engineer",
        "address": "module_1",
        "email": "maxmerkuriy@mars.com",
        "city_from": "Penza"
    },
    {
        "surname": "Sotina",
        "name": "Alyona",
        "age": 18,
        "position": "scientist",
        "speciality": "scientist",
        "address": "module_1",
        "email": "alena_sotona@mars.org",
        "city_from": "Moscow"
    },
    {
        "surname": "Ivanov",
        "name": "Ivan",
        "age": 30,
        "position": "worker",
        "speciality": "mechanic",
        "address": "module_2",
        "email": "russian_ivan@mars.ru",
        "city_from": "Omsk"
    }
)

db_session.global_init("db/mars_explorer.db")
session = db_session.create_session()
for ast in ast_data:
    user = user_table.User()
    user.surname = ast["surname"]
    user.name = ast["name"]
    user.age = ast["age"]
    user.position = ast["position"]
    user.speciality = ast["speciality"]
    user.address = ast["address"]
    user.email = ast["email"]
    user.city_from = ast["city_from"]
    session.add(user)
session.commit()
