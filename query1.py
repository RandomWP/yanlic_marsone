from data.db_session import global_init, create_session
from data.user_table import User

filename = input()
global_init(filename)
session = create_session()
users = session.query(User).filter(User.address == "module_1")
for user in users:
    print(user)