import flask
import flask_login
import flask_restful
import os
import sys
import job_api
import user_api
import requests
from resources import user_resource, job_resource
from data import db_session, job_table, user_table, register_form, category_table
from data import login_form, addjob_form, department_table, adddep_form


class YaMapSearch(object):
    def __init__(self):
        self.err = False
        self.server_addr = "https://geocode-maps.yandex.ru/1.x/"
        self.apikey = "40d1649f-0493-4b70-98ba-98533de7710b"

    def search_address(self, address: str):
        self.geocode = address
        self.kind = ""
        self._request()

    def search_ll(self, ll: tuple, kind=""):
        self.geocode = ",".join(map(str, ll))
        self.kind = kind
        self._request()

    def get_ll(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        ll_string = feature_member["GeoObject"]["Point"]["pos"]
        return tuple(map(float, ll_string.split()))

    def get_address(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["formatted"]

    def get_text(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"]

    def get_point(self, index: int, style: str, color: str = "", size: int = "", content: int = ""):
        ll = self.get_ll(index)
        return YaMapPoint(ll, style, color, size, content)

    def get_postal_code(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        try:
            return feature_member["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["postal_code"]
        except KeyError:
            return ""

    def get_results_count(self):
        return int(self.json_resp["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["found"])

    def get_spn(self, index: int):
        feature_member = self.json_resp["response"]["GeoObjectCollection"]["featureMember"][index]
        toponym_delta_1 = list(map(
            float, feature_member["GeoObject"]["boundedBy"]["Envelope"]['lowerCorner'].split()))
        toponym_delta_2 = list(map(
            float, feature_member["GeoObject"]["boundedBy"]["Envelope"]['upperCorner'].split()))
        delta1 = str(abs(toponym_delta_1[0] - toponym_delta_2[0]))
        delta2 = str(abs(toponym_delta_1[1] - toponym_delta_2[1]))
        return (delta1, delta2)

    def _request(self):
        req_params = {
            "apikey": self.apikey,
            "geocode": self.geocode,
            "format": "json"
        }
        if self.kind:
            req_params["kind"] = self.kind
        response = requests.get(self.server_addr, req_params)
        if not response:
            print("here")
            self.err = True
            self.status_code = response.status_code
            self.reason = response.reason
        self.err = False
        self.json_resp = response.json()

    def is_error(self):
        return self.err

    def req_status(self):
        return (self.status_code, self.reason)


app = flask.Flask(__name__)
app.config["SECRET_KEY"] = "Bj2iJ9cQz3EXqvLjEQ"
app.register_blueprint(job_api.blueprint)
app.register_blueprint(user_api.blueprint)
api = flask_restful.Api(app)
api.add_resource(user_resource.UserResource, "/api/v2/users/<int:user_id>")
api.add_resource(user_resource.UserListResource, "/api/v2/users")
api.add_resource(job_resource.JobResource, "/api/v2/jobs/<int:job_id>")
api.add_resource(job_resource.JobListResource, "/api/v2/jobs")
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}


@login_manager.user_loader
def load_user(user_id):
    session = db_session.create_session()
    return session.query(user_table.User).get(user_id)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/index")
def index():
    session = db_session.create_session()
    jobs = session.query(job_table.Jobs).all()
    return flask.render_template("works_log.html", jobs=jobs)


@app.route("/")
def root():
    return flask.redirect("/index")


@app.route("/list_prof/<list_type>")
def list_prof(list_type):
    jobs = ("инженер-исследователь",
            "пилот",
            "строитель",
            "экзобиолог",
            "врач",
            "инженер по терраформированию",
            "климатолог",
            "специалист по радиационной защите",
            "астрогеолог", "гляциолог",
            "инженер жизнеобеспечения",
            "метеоролог",
            "оператор марсохода",
            "киберинженер",
            "штурман",
            "пилот дронов")
    return flask.render_template("list_prof.html", list_type=list_type, jobs=jobs)


@app.route("/gallery", methods=("GET", "POST"))
def gallery():
    gallery_path = "static/img/gallery"
    if flask.request.method == "GET":
        slides = map(lambda filname: os.path.join(
            gallery_path, filname), os.listdir(gallery_path))
        return flask.render_template("gallery.html", slide_paths=slides)
    elif flask.request.method == "POST":
        print("post")
        if 'file' not in flask.request.files:
            print("err1")
            flask.flash('Ошибка загрузки', "error")
            return flask.redirect(flask.request.url)
        f = flask.request.files["file"]
        if not f.filename:
            print("err2")
            flask.flash("Файл не выбран", "error")
            return flask.redirect(flask.request.url)
        if f and allowed_file(f.filename):
            print("here")
            last_slide = max(map(int, map(lambda fname: fname.rsplit(".")[
                             0][5:], os.listdir(gallery_path))))
            new_name = f"slide{last_slide + 1}.{f.filename.rsplit('.')[1]}"
            print(new_name)
            f.save(os.path.join(gallery_path, new_name))
            flask.flash("Картинка загружена", "info")
            return flask.redirect(flask.request.url)


@app.route("/register", methods=("GET", "POST"))
def register():
    form = register_form.RegisterForm()
    print(form.validate_on_submit())
    if form.validate_on_submit():
        if form.password.data != form.password_again.data:
            return flask.render_template("register.html", form=form, message="Passwords must match")
        session = db_session.create_session()
        if session.query(user_table.User).filter(user_table.User.email == form.email.data).first():
            return flask.render_template("register.html", form=form, message="This email is already taken")
        user = user_table.User(
            email=form.email.data,
            surname=form.surname.data,
            name=form.name.data,
            age=form.age.data,
            position=form.position.data,
            speciality=form.speciality.data,
            address=form.address.data
        )
        user.set_passwd(form.password.data)
        session.add(user)
        session.commit()
        return flask.redirect("/login")
    return flask.render_template("register.html", form=form, title="Register")


@app.route("/login", methods=("GET", "POST"))
def login():
    form = login_form.LoginForm()
    if form.validate_on_submit():
        session = db_session.create_session()
        user = session.query(user_table.User).filter(
            user_table.User.email == form.email.data).first()
        if user and user.check_passwd(form.password.data):
            flask_login.login_user(user, remember=form.remember.data)
            return flask.redirect("/index")
        return flask.render_template("login.html", message="Invalid login or email", form=form, title="Login")
    return flask.render_template("login.html", form=form, title="Login")


@app.route("/addjob", methods=("GET", "POST"))
@flask_login.login_required
def addjob():
    form = addjob_form.AddJobForm()
    if form.validate_on_submit():
        session = db_session.create_session()
        category = session.query(category_table.Category).filter(
            category_table.Category.id == form.category.data).first()
        print(category)
        job = job_table.Jobs()
        job.job = form.job.data
        job.team_leader = form.team_leader.data
        job.work_size = form.work_size.data
        job.collaborators = form.collaborators.data
        job.is_finished = form.is_finished.data
        job.categories.append(category)
        session.add(job)
        session.merge(flask_login.current_user)
        session.commit()
        return flask.redirect("/index")
    return flask.render_template("addjob.html", form=form, title="Add job")


@app.route("/editjob/<int:job_id>", methods=("GET", "POST"))
@flask_login.login_required
def editjob(job_id):
    session = db_session.create_session()
    job = session.query(job_table.Jobs).filter(
        job_table.Jobs.id == job_id).first()
    if not job:
        flask.abort(404)
    elif flask_login.current_user.id != 1 and flask_login.current_user != job.user:
        flask.abort(403)
    else:
        form = addjob_form.AddJobForm()
        if form.validate_on_submit():
            category = session.query(category_table.Category).filter(
                category_table.Category.id == form.category.data).first()
            job.job = form.job.data
            job.team_leader = form.team_leader.data
            job.work_size = form.work_size.data
            job.collaborators = form.collaborators.data
            job.is_finished = form.is_finished.data
            job.categories = [category]
            session.commit()
            return flask.redirect("/index")
        form.job.data = job.job
        form.team_leader.data = job.team_leader
        form.work_size.data = job.work_size
        form.collaborators.data = job.collaborators
        form.is_finished.data = job.is_finished
        form.category.data = job.categories[0].id
        return flask.render_template("addjob.html", form=form, title="Edit job")


@app.route("/deljob/<int:job_id>")
@flask_login.login_required
def deljob(job_id):
    session = db_session.create_session()
    job = session.query(job_table.Jobs).filter(
        job_table.Jobs.id == job_id).first()
    if not job:
        flask.abort(404)
    elif flask_login.current_user.id != 1 and flask_login.current_user != job.user:
        flask.abort(403)
    else:
        session.delete(job)
        session.commit()
        return flask.redirect("/index")


@app.route("/departments")
def departments():
    session = db_session.create_session()
    departments = session.query(department_table.Departments).all()
    return flask.render_template("departments.html", departments=departments, title="Departments")


@app.route("/adddep", methods=("GET", "POST"))
@flask_login.login_required
def adddep():
    form = adddep_form.AddDepForm()
    if form.validate_on_submit():
        session = db_session.create_session()
        if session.query(department_table.Departments).filter(department_table.Departments.email == form.email.data).first():
            return flask.render_template("adddep.html", form=form, title="Add department", message="This email is already taken")
        department = department_table.Departments()
        department.title = form.title.data
        department.chief = form.chief.data
        department.members = form.members.data
        department.email = form.email.data
        flask_login.current_user.departments.append(department)
        session.merge(flask_login.current_user)
        session.commit()
        return flask.redirect("/departments")
    return flask.render_template("adddep.html", form=form, title="Add department")


@app.route("/editdep/<int:dep_id>", methods=("GET", "POST"))
@flask_login.login_required
def editdep(dep_id: int):
    session = db_session.create_session()
    department = session.query(department_table.Departments).filter(
        department_table.Departments.id == dep_id).first()
    if not department:
        flask.abort(404)
    elif flask_login.current_user.id != 1 and flask_login.current_user != department.user:
        flask.abort(403)
    else:
        form = adddep_form.AddDepForm()
        if form.validate_on_submit():
            department.title = form.title.data
            department.chief = form.chief.data
            department.members = form.members.data
            department.email = form.email.data
            session.commit()
            return flask.redirect("/departments")
        form.title.data = department.title
        form.chief.data = department.chief
        form.members.data = department.members
        form.email.data = department.email
        return flask.render_template("adddep.html", form=form, title="Edit department")


@app.route("/deldep/<int:dep_id>")
@flask_login.login_required
def deldep(dep_id: int):
    session = db_session.create_session()
    department = session.query(department_table.Departments).filter(
        department_table.Departments.id == dep_id).first()
    if not department:
        flask.abort(404)
    elif flask_login.current_user.id != 1 and flask_login.current_user != department.user:
        flask.abort(403)
    else:
        session.delete(department)
        session.commit()
        return flask.redirect("/departments")


@app.route("/logout")
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return flask.redirect("/index")


@app.route("/users_show/<int:user_id>")
def users_show(user_id: int):
    resp = requests.get(f"http://localhost:8080/api/v2/users/{user_id}")
    print(resp.content)
    json_resp = resp.json()
    hometown = json_resp["user"]["city_from"]
    search = YaMapSearch()
    search.search_address(hometown)
    ll = ",".join(map(str, search.get_ll(0)))
    spn = ",".join(map(str, search.get_spn(0)))
    map_url = f"https://static-maps.yandex.ru/1.x/?lang=en_US&ll={ll}&spn={spn}&l=sat"
    return flask.render_template("users_show.html", map_url=map_url, user=json_resp["user"])


def main():
    db_session.global_init("db/mars_explorer.db")
    app.run(host="127.0.0.1", port=8080)


if __name__ == "__main__":
    main()
